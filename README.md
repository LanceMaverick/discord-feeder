# Discord Feeder
### Post articles to specific channels via rss feeds when there's an update

## Setup

Get the code
```
git clone https://gitlab.com/LanceMaverick/discord-feeder.git
cd discord-feeder
```

Install requirements
``` pip install -r requirements.txt ```


Create the config file
```
cp config.py.example config.py
```

Add feeds to the list `feeds` in config.py with the following form

```{'name': feed_name, 'rss:' rss_feed_url, 'webhook': discord_webhook_url}```

As many feeds as you wish can be added.
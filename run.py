import time
from config import feeds
from feeder import Feed
test_url = 'https://store.steampowered.com/feeds/news.xml'

feed_objects = [Feed(f['name'], f['rss'], f['webhook']) for f in feeds]

while True:
    for f in feed_objects:
        u = f.update()
        if u:
            print(u)
    time.sleep(600)




from time import mktime
from datetime import datetime
import requests
import json
import feedparser
import dataset

class Feed:
    def __init__(self, name, rss, webhook, db_url=None):
        self.name = name
        self.rss = rss
        self.webhook = webhook
        self.data = None
        if not db_url:
            self.db_url = 'sqlite:///feed.db'
        else:
            self.db_url = db_url
    
    def update_data(self):
        self.data = feedparser.parse(self.rss)
        return self.data

    def get_new_posts(self):
        self.update_data()
        new_posts = []
        for post in self.data.entries:
            with dataset.connect(self.db_url) as db:
                table = db[self.name]
                if not table.find_one(link=post.link):
                    new_post = dict(
                        pid = post.id,
                        title = post.title,
                        link = post.link,
                        date = datetime.fromtimestamp(mktime(post.published_parsed)))                   
                    new_posts.append(new_post)
                    table.insert(new_post)
                    db.commit()
                    
        if new_posts:
            print('New posts detected for {}'.format(self.name)) 
            return dict(new_posts=new_posts, newest = max(new_posts, key=lambda x:x['date']))

    def send_hook(self, post_data=None):
        payload = dict(
            content = '**{}** \n {}'.format(post_data['title'], post_data['link']),
        )
        if not self.webhook:
            return payload
        response = requests.post(
            self.webhook,
            data = json.dumps(payload),
            headers={'Content-Type': 'application/json'})
        return response

    def update(self):
        new_posts = self.get_new_posts()
        if new_posts:
            newest = new_posts['newest']
            return self.send_hook(newest)
        else:
            return None